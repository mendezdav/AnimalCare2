package com.app.animalcare;

import java.util.ArrayList;
import java.util.List;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.os.Build;
import android.widget.AdapterView.OnItemClickListener;
import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class ListaVeterinarios extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		final ArrayList<String> nombre = new ArrayList<String>();
		final ArrayList<String> telefono = new ArrayList<String>();
		final ArrayList<String> direccion = new ArrayList<String>();
		final ArrayList<String> correo = new ArrayList<String>();
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lista_veterinarios);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		 DatabaseHandler db = new DatabaseHandler(ListaVeterinarios.this);
		 List<Veterinarios> listVeterinarios = db.getAllVete();
				
		 for (Veterinarios v : listVeterinarios )
		 {
				 Log.d("Nombre", v.get_nombre());
				 nombre.add(v.get_nombre());	 
				 telefono.add(v.get_telefono());
				 correo.add(v.get_correo());
				 direccion.add(v.get_direccion());
		 }
			 
		ListView listar = (ListView) findViewById(R.id.Lista_Vet);
		listar.setAdapter(new ArrayAdapter<String>(this,R.layout.options_list, R.id.optionItem,
		    		 nombre)); 
		 /*
		  * seleccion de un elemento de la lista
		  */
		 ListView listarView = (ListView) findViewById(R.id.Lista_Vet);
		 listarView.setOnItemClickListener(new OnItemClickListener() {
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					Log.d("Posicion", position + "");
					Log.d("nombre", nombre.get(position));
					Intent accion = new Intent(getApplicationContext(), VetProfile_.class);
					accion.putExtra("nombre",nombre.get(position));
					accion.putExtra("telefono",telefono.get(position));
					accion.putExtra("direccion",direccion.get(position));
					accion.putExtra("correo",correo.get(position));
					startActivity(accion);
				}
			});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.lista_veterinarios, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(
					R.layout.fragment_lista_veterinarios, container, false);
			return rootView;
		}
	}

}
