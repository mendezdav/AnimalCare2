package com.app.animalcare;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class AdaptadorListaMascotas extends BaseAdapter{
	Activity context;
	ArrayList<ListaMascotas> datos;
	AdaptadorListaMascotas(Activity context, ArrayList<ListaMascotas> datos){
		this.context = context;
		this.datos = datos;
	}
	/**Retornando item**/
	@Override
	public Object getItem(int arg0){
		return datos.get(arg0);
	}
	/**Retornando Id de mascota**/
	@Override
	public long getItemId(int position){
		return datos.get(position).get_idMascota();
	}
	
	public View getView(int position, View vista, ViewGroup parent){
		View v = vista;
		if(vista == null){
			LayoutInflater inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inf.inflate(R.layout.list_item_mascotas, null);
		}//fin if
		//Instanciando clase
		ListaMascotas lm = datos.get(position);
		//Obteniendo la foto
		ImageView foto = (ImageView) v.findViewById(R.id.pic_lista);
		foto.setImageBitmap(lm.getPic());
		TextView nombre = (TextView) v.findViewById(R.id.nombre_lista);
		nombre.setText(lm.get_nombre());
		
		TextView genero = (TextView) v.findViewById(R.id.genero_lista);
		genero.setText(lm.get_genero());
		
		return v;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return datos.size();
	}
}
