package com.app.animalcare;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class PetProfile extends ActionBarActivity {

	private int id_mascota;
	private String pic_intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_pet_profile);
        /**Estableciendo las propiedades***/
        TextView nombreMascota = (TextView) findViewById(R.id.nombre);
        //TextView edadMascota   = (TextView) findViewById(R.id.edad);
        TextView razaMascota   = (TextView) findViewById(R.id.raza);
        TextView genero        = (TextView) findViewById(R.id.genero);
        TextView especie 	   = (TextView) findViewById(R.id.especie);
        TextView apodo		   = (TextView) findViewById(R.id.apodo);
        ImageView pic		   = (ImageView) findViewById(R.id.profilePic);
        
        /**Capturando datos***/
        String nombre_intent = getIntent().getExtras().getString("NOMBRE");
		String apodo_intent  = getIntent().getExtras().getString("APODO");
		String edad_intent = getIntent().getExtras().getString("EDAD");
		String especie_intent = getIntent().getExtras().getString("ESPECIE");
		String raza_intent = getIntent().getExtras().getString("RAZA");
		String genero_intent = getIntent().getExtras().getString("GENERO");	
		pic_intent =  getIntent().getExtras().getString("PIC");
		id_mascota = getIntent().getExtras().getInt("ID");
		
		
		Bitmap pic_bitmap = (Bitmap) BitmapFactory.decodeFile(pic_intent);
		/**
		 * Estableciendo los datos
		 */
		nombreMascota.setText(nombre_intent);
		apodo.setText(apodo_intent);
		especie.setText(especie_intent);
		genero.setText(genero_intent);
		razaMascota.setText(raza_intent);
//		edadMascota.setText(edad_intent);
		pic.setImageBitmap(pic_bitmap);

		if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
		
		String[] events = {"Nuevo Recordatorio"};
        ListView eventList = (ListView) findViewById(R.id.optionList);
        ArrayAdapter<String> eventAdapter = new ArrayAdapter<String>(this, R.layout.options_list, R.id.optionItem, events);
        eventList.setAdapter(eventAdapter);
        
        eventList.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?> arg0, View arg1, int posicion,
					long arg3) {
				if(posicion==0){
					Intent perfil = new Intent(getApplicationContext(), MenuRecordatorio.class);
					perfil.putExtra("id_mascota", id_mascota);
					startActivity(perfil);
				}
			}
		});
      
    }
    /**
     * Parte de Imagen
     */
    public void agrandar(View v){ 
    	Intent foto = new Intent(getApplicationContext(), Foto_vista.class);
    	foto.putExtra("pic", this.pic_intent);
    	startActivity(foto);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.pet_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_pet_profile, container, false);
            return rootView;
        }
    }

}
