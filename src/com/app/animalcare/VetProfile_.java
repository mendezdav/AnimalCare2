package com.app.animalcare;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.os.Build;

public class VetProfile_ extends ActionBarActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vet_profile_);
		setTitle("Directorio veterinario");
		
		/**Estableciendo propiedades**/
		String nombrevet;
		String emailvet;
		final String telefonovet;
		final String direccionvet;
		/***Inicializando componentes***/
		TextView txtnombrevet = (TextView) findViewById(R.id.nombreVet);
		TextView txtemailvet  = (TextView) findViewById(R.id.emailvet_);
		TextView txtdireccion = (TextView) findViewById(R.id.direccionvet);
		
		
		nombrevet = getIntent().getExtras().getString("nombre");
		emailvet = getIntent().getExtras().getString("correo");
		telefonovet = getIntent().getExtras().getString("telefono");
		direccionvet = getIntent().getExtras().getString("direccion");
		
		txtnombrevet.setText(nombrevet);
		txtemailvet.setText(emailvet);
		txtdireccion.setText(direccionvet);
		
		ImageButton btnllamar = (ImageButton) findViewById(R.id.callvet);
		Button btnmapa = (Button) findViewById(R.id.btnmapa);
		
		btnmapa.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				String dirNueva = direccionvet.replace(" ", "+");
				Intent abrirMapa = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q="+dirNueva));
				startActivity(abrirMapa);
			}
		});
	    btnllamar.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent llamada = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+telefonovet));
				startActivity(llamada);
				
			}
		});
		
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.vet_profile_, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_vet_profile_,
					container, false);
			return rootView;
		}
	}

}
