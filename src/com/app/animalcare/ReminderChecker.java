package com.app.animalcare;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

/**
 * Servicio para consulta de base de datos en background y generacion de notificaciones
 * */

public class ReminderChecker extends Service implements Runnable{
	
	private NotificationManager nManager;
	private Timer timer = new Timer();
	private static final long UPDATE_INTERVAL = 5000;
	
	@Override public void onCreate() {
	   super.onCreate();
	   run(); // el metodo run jamas se llamo implicitamente, se hace llamada explicita 
	 }
	
	@Override public int onStartCommand(Intent intent, int flags, int startId){
		run(); // el metodo run jamas se llamo implicitamente, se hace llamada explicita
		return START_STICKY;
	}

	/**
	 * revision y notificacion
	 * */
	private void Notificar(){
	    int event = -1; // id del recordatorio
	    
	    MascotaDB helperDB = MascotaDB.getInstance(this); // obtiene una instancia del manejador para la base de datos
	    
	    // verifica si existe algun evento activo, retorna -1 en caso de no existir
	    event = helperDB.isEvent(); 
		
	    // si event > 0 entonces se ha encontrado algun recordatorio para la fecha y hora actual, es decir que es hora de alertar
	    if(event >= 0){
			Context context = this; // contexto
			
			Intent i = new Intent();
	
			PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
			
			// soporte para android < API 11
			NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		    
			builder.setContentTitle("Recordatorio"); // titulo de notificacion
			
		    builder.setContentText(helperDB.getReminder(event)); // obtiene la descripcion del recordatorio
		    
		    builder.setContentIntent(pendingIntent);
		    
		    builder.setSmallIcon(R.drawable.ic_launcher); // icono de app para notificacion
		    
		    builder.setWhen(System.currentTimeMillis()); // lanza la notificacion al instante
		    
		    // activa sonido y vibracion, la vibracion requiere permisos, para ver los permisos consulta AndroidManifest.xml
		    builder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE );
		    
		    //PendingIntent contentIntent = PendingIntent.getActivity(context, 0,i, PendingIntent.FLAG_UPDATE_CURRENT);
		    //builder.setContentIntent(contentIntent);
		    
		    // obtiene un servicio del sistema para notificacion
		    nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		    
		    // lanza la notificacion con id == id del recordatorio
		    nManager.notify(event, builder.build());
		}
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		// generar un hilo para no colgar la aplicacion u otros procesos incluyendo la GUI
		new Thread(new Runnable() {
		    public void run() {
		       timer.scheduleAtFixedRate(new TimerTask(){
		    	   
		    	   @Override public void run(){
		    		   Notificar();
		    	   }
		    	   
		       }, 0, UPDATE_INTERVAL);
		    }
		}).start();
	}
}
