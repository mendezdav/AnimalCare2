package com.app.animalcare;

import java.util.ArrayList;
import java.util.Iterator;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ListaRecordatorio extends ActionBarActivity {

	private int mascota_activa;
	private Integer recIntBuf[];
	private int selected_rec;
	private RecordatorioAdapter eventAdapter;
	private ListView list;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lista_recordatorio);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		
		// procedo a obtener el nombre de la mascota 
		Intent intent = getIntent();
		mascota_activa = intent.getIntExtra("mascota",0);
		MascotaDB helperDB    = MascotaDB.getInstance(this);
		// obtenerNombre devuelve "" si existe algun error interno
		String nombre_mascota = helperDB.obtenerNombre(mascota_activa);
		
		// trata de obtener los recordatorios
		try{
			ArrayList<Integer> recordatorios = helperDB.obtenerRecordatorios(mascota_activa);
			
			recIntBuf = convertIntegers(recordatorios); 
			
			if(recIntBuf.length == 0){
				findViewById(R.id.emptyList).setVisibility(1);
			}
		}catch(Exception e){
			findViewById(R.id.emptyList).setVisibility(1);
			((TextView)findViewById(R.id.emptyList)).setText("Lo sentimos, no pudimos cargar sus recordatorios en este momento :(");
			recIntBuf = new Integer[0];
		}
		
		
		setTitle("Recodatorios para "+nombre_mascota); // cambia titulo de la vista
        
		// crea el listview con su adapter y con eventos para menu contextual
		list = (ListView) findViewById(R.id.lista_recordatorios);
        eventAdapter = new RecordatorioAdapter(this, recIntBuf);
        list.setAdapter(eventAdapter);
        registerForContextMenu(list);
	}
	
	/**
	 * Crea le menu contextual
	 * */
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	                                ContextMenuInfo menuInfo) {
	    super.onCreateContextMenu(menu, v, menuInfo);
	    MenuInflater inflater = getMenuInflater();
	    TextView reg = (TextView)v.findViewById(R.id.id_reg);
	    selected_rec = Integer.parseInt(reg.getText().toString());
	    inflater.inflate(R.menu.context_menu_rec, menu);
	}
	
	/**
	 * Muestra un dialogo de confirmacion de borrado
	 * */
	@Override
	public boolean onContextItemSelected(MenuItem item) {
	    AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
	    switch (item.getItemId()) {
	        case R.id.delete_option_rec:
	        	selected_rec = info.position;
	        	ConfirmDeleteDialog inst = new ConfirmDeleteDialog();
	        	inst.show(getSupportFragmentManager(),"Eliminar");
	            return true;
	        default:
	            return super.onContextItemSelected(item);
	    }
	}
	
	/** 
	 * Elimina recordatorio, invocado desde dialogo de borrado
	 * */
	public void eliminarRecordatorio(){
		Context context   = getApplicationContext();
		CharSequence text = "Registro eliminado"; 
		int duration = Toast.LENGTH_SHORT;
    	try{
    		MascotaDB helperDB    = MascotaDB.getInstance(this);
        	helperDB.eliminarRecordatorio(recIntBuf[selected_rec]);
        	ArrayList<Integer> recordatorios = helperDB.obtenerRecordatorios(mascota_activa);
    		recIntBuf = convertIntegers(recordatorios);
    		eventAdapter = new RecordatorioAdapter(this, recIntBuf);
            list.setAdapter(eventAdapter);
            list.invalidate();
            if(recIntBuf.length == 0){
    			findViewById(R.id.emptyList).setVisibility(1);
    		}
    	}catch(Exception e){
    		text = "No se pudo eliminar";
    	}
        
        Toast success = Toast.makeText(context, text, duration);
		success.show();
	}
	
	/**
	 * Simple funcion de conversion
	 * */
	public static Integer[] convertIntegers(ArrayList<Integer> integers)
	{
		try{
		    Integer[] ret = new Integer[integers.size()];
		    Iterator<Integer> iterator = integers.iterator();
		    for (int i = 0; i < ret.length; i++)
		    {
		        ret[i] = iterator.next().intValue();
		    }
		    return ret;
		}catch(Exception e){
			Log.i("Null", "Error en conversion");
			return new Integer[0];
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.lista_recordatorio, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(
					R.layout.fragment_lista_recordatorio, container, false);
			return rootView;
		}
	}

}
