package com.app.animalcare;

import java.util.ArrayList;
import java.util.List;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class AnimalCare extends ActionBarActivity {
	final String ruta = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/animalCare/";
	List<Mascotas> datos;
	int selected_rec;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("Mis mascotas");
		setContentView(R.layout.activity_principal);
		/**Obteniendo la lista de mascotas***/
		DatabaseHandler db = new DatabaseHandler(this);
		datos = db.getAllPET();
		ArrayList<ListaMascotas> items = new ArrayList<ListaMascotas>();
		
		ListaMascotas item; //Objeto general
		ListView lista = (ListView) findViewById(R.id.mascotas_lista);
		
		String nombre_img;
		/**Llenando ***/
		for (Mascotas cn : datos) {
			nombre_img = cn.get_foto();
			item = new ListaMascotas(BitmapFactory.decodeFile(nombre_img), cn.get_idMascota(), cn.get_nombre(), cn.get_apodo(),
					cn.get_genero());
			items.add(item);
		}
		//Estableciendo el adaptador
		AdaptadorListaMascotas adaptador = new AdaptadorListaMascotas(this, items);
		//Mostrando en la lista	

		lista.setAdapter(adaptador);
	
		if(!isMyServiceRunning(ReminderChecker.class)){
			Intent serviceIntent = new Intent(this, ReminderChecker.class);
			this.startService(serviceIntent);
		}
		
		lista.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(AdapterView<?> arg0, View arg1, int posicion,
					long arg3) {
					Intent perfil = new Intent(getApplicationContext(), PetProfile.class);
					//perfil.putExtra("PIC", datos.get(posicion).get);
					perfil.putExtra("NOMBRE", datos.get(posicion).get_nombre());
					perfil.putExtra("APODO", datos.get(posicion).get_apodo());
					perfil.putExtra("FECHA_NACIMIENTO", datos.get(posicion).get_fechaNacimiento());
					perfil.putExtra("ESPECIE", datos.get(posicion).get_especie());
					perfil.putExtra("RAZA", datos.get(posicion).get_raza());
					perfil.putExtra("GENERO", datos.get(posicion).get_genero());
					perfil.putExtra("EDAD", datos.get(posicion).get_edad());
					perfil.putExtra("PIC", datos.get(posicion).get_foto());
					perfil.putExtra("ID", datos.get(posicion).get_idMascota());
					startActivity(perfil);
			}
		});
		
		registerForContextMenu(lista);
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
	                                ContextMenuInfo menuInfo) {
	    super.onCreateContextMenu(menu, v, menuInfo);
	    MenuInflater inflater = getMenuInflater();
	    TextView reg = (TextView)v.findViewById(R.id.id_reg);
	    inflater.inflate(R.menu.context_menu_pet, menu);
	}

	/**
	 * Muestra un dialogo de confirmacion de borrado
	 * */
	@Override
	public boolean onContextItemSelected(MenuItem item) {
	    AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
	    switch (item.getItemId()) {
	        case R.id.delete_option_rec:
	        	selected_rec = info.position;
	        	deletePet inst = new deletePet();
	        	inst.show(getSupportFragmentManager(),"Eliminar");
	            return true;
	        default:
	            return super.onContextItemSelected(item);
	    }
	}
	
	public void eliminarMascota(){
		Context context   = getApplicationContext();
		CharSequence text = "Registro eliminado"; 
		int duration = Toast.LENGTH_SHORT;
    	try{
    		MascotaDB helperDB    = MascotaDB.getInstance(this);
        	helperDB.eliminarMascota(datos.get(selected_rec).get_idMascota());
        	DatabaseHandler db = new DatabaseHandler(this);
        	datos = db.getAllPET();
        	ArrayList<ListaMascotas> items = new ArrayList<ListaMascotas>();
    		ListaMascotas item; //Objeto general
    		ListView lista = (ListView) findViewById(R.id.mascotas_lista);
    		String nombre_img;
    		/**Llenando datos***/
    		for (Mascotas cn : datos) {
    			nombre_img = cn.get_foto();
    			item = new ListaMascotas(BitmapFactory.decodeFile(nombre_img), cn.get_idMascota(), cn.get_nombre(), cn.get_apodo(),
    					cn.get_genero());
    			items.add(item);
    		}
    		//Estableciendo el adaptador
    		AdaptadorListaMascotas adaptador = new AdaptadorListaMascotas(this, items);
    		//Mostrando en la lista	

    		lista.setAdapter(adaptador);
    		lista.invalidate();
        	//ArrayList<Integer> recordatorios = helperDB.obtenerRecordatorios(mascota_activa);
    		//recIntBuf = convertIntegers(recordatorios);
    		//eventAdapter = new RecordatorioAdapter(this, recIntBuf);
            //list.setAdapter(eventAdapter);
            //list.invalidate();
            
    	}catch(Exception e){
    		text = "No se pudo eliminar";
    	}
        
        Toast success = Toast.makeText(context, text, duration);
		success.show();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.principal, menu);
		return true;
	}
	public boolean onOptionsItemSelected(MenuItem item) {
	        // Handle action bar item clicks here. The action bar will
	        // automatically handle clicks on the Home/Up button, so long
	        // as you specify a parent activity in AndroidManifest.xml.
	        int id = item.getItemId();
	        if (id == R.id.action_nuevo) {
	              Intent newPet = new Intent(getApplicationContext(), NuevaMascota.class);
	              startActivity(newPet);
	        }
	        if (id == R.id.action_ver_veterinarios) {
	        	
	        	/*
	        	 * Revisar si ya hay datos de veterinarios
	        	 */
	        	
	       	 DatabaseHandler db = new DatabaseHandler(AnimalCare.this);
			 int contador = db.EmptyVet();
			 if (contador == 0 )
			 {
				 Veterinarios vet = new Veterinarios("Carlo Enrique Sandoval","7165-3201","Colonia Zacamil, Mejicanos, San Salvador, El Salvador"
						 ,"CarloSandoval@animalCare.com");
				 Veterinarios vet2 = new Veterinarios("Policlínica Veterinaria", "22980324", "Col Avila 63 Av Sur y Pje Sta Mónica No 17, El Salvador, San Salvador ", 
						 "www.policlinicaveterinaria.net");
				 Veterinarios vet3 = new Veterinarios("CLÍNICA VETERINARIA TECANA ", "22464546", "Residencial Madreselva II Calle Conchagua Pte No 22, El Salvador, Antiguo Cuscatlán ", 
						 "www.vettecana.com");
				 Veterinarios vet4 = new Veterinarios("PETERINARIA PELITOS", "22740405", "Ps y Col Miralvalle No 276,El Salvador, San Salvador ", "www.pelitos.com.sv");
				 
				 db.addVeterinario(vet);
				 db.addVeterinario(vet2);
				 db.addVeterinario(vet3);
				 db.addVeterinario(vet4);
				
			 }
			 
	              Intent verVet = new Intent(getApplicationContext(), ListaVeterinarios.class);
	              startActivity(verVet);
	        }
	        
	        return super.onOptionsItemSelected(item);
	}
	
	private boolean isMyServiceRunning(Class<?> serviceClass) {
	    ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
	    for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
	        if (serviceClass.getName().equals(service.service.getClassName())) {
	            return true;
	        }
	    }
	    return false;
	}

}
