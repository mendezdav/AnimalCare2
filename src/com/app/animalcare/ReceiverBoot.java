package com.app.animalcare;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ReceiverBoot extends BroadcastReceiver{

	@Override public void onReceive(Context context, Intent intent) {
		/* 
		 * el evento ON_BOOT_COMPLETED se establece en el android manifest,
		 * este evento se ejecuta cada vez que se enciende el dispositivo
		 * y permite inicial un nuevo servicio sin que la app haya sido ejecutada
		 * ideal para generar notificaciones y actualizaciones de datos
		 * 
		 *  NOTA: para ver el registros de este receiver, consultar AndroidManifest.xml
		 * 
		 * */
		
		// lanza un nuevo servicio, este servicio debe estar registrado en  AndroidManifest.xml
		Intent serviceIntent = new Intent(context, ReminderChecker.class);
		context.startService(serviceIntent);
	}
	
}
