package com.app.animalcare;

public class Veterinarios {
	private int _id;
	private String _nombre;
	private String _telefono;
	private String _direccion;
	private String _correo;
		
	public Veterinarios(){ }
	
	// constructor con ID
	public Veterinarios(int id,String nombre, String telefono, String direccion,String correo)
	{
		this._id = id;
		this._nombre = nombre;
		this._telefono = telefono;
		this._direccion = direccion;
		this._correo = correo;
	}
	
	// constructor
	public Veterinarios(String nombre, String telefono, String direccion,String correo)
	{
		this._nombre = nombre;
		this._telefono = telefono;
		this._direccion = direccion;
		this._correo = correo;
	}
	
	
	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String get_nombre() {
		return _nombre;
	}

	public void set_nombre(String _nombre) {
		this._nombre = _nombre;
	}

	public String get_telefono() {
		return _telefono;
	}

	public void set_telefono(String _telefono) {
		this._telefono = _telefono;
	}

	public String get_direccion() {
		return _direccion;
	}

	public void set_direccion(String _direccion) {
		this._direccion = _direccion;
	}

	public String get_correo() {
		return _correo;
	}

	public void set_correo(String _correo) {
		this._correo = _correo;
	}
	
}
