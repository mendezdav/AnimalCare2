package com.app.animalcare;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class MascotaDB extends SQLiteOpenHelper {

	private static MascotaDB instance;
	private String descripcion;
	private String hora;
	private String f_inicio;
	private String f_final;
	private int intervalo;
	
	/**
	 * constructor
	 * */
	public MascotaDB(Context context) {
		super(context, "MascotasManager", null, 1);
		// TODO Auto-generated constructor stub
		descripcion = "";
		hora        = "";
		f_inicio    = "";
		f_final     = "";
		intervalo   = 0;
	}
	
	/**
	 * 
	 * Metodos para acceder a datos de recordatorio
	 * 
	 * */
	public String obtenerDescripcion(){
		
		return descripcion;
	}
	
	public String obtenerFechaInicio(){
		
		return f_inicio;
	}
	
	public String obtenerFechaFinal(){
		
		return f_final;
	}
	
	public String obtenerHora(){
		
		return hora;
	}
	
	public int obtenerIntervalo(){
		
		return intervalo;
	}
	
	/** FIN metodos para obtener datos recordatorio */
	
	
	/** 
	 * Permite usar la misma instancia para manejar varias conexiones 
	 * */
	public static synchronized MascotaDB getInstance(Context context){
		if( instance==null)
			instance = new MascotaDB(context);
		return instance;
	}

	/**
	 * Elimina un recordatorio
	 * */
	public void eliminarRecordatorio(int rec){
		// no posee bloque try-catch, se debe hacer al momento de invocar el metodo
		String deleteQuery = "DELETE FROM recordatorio WHERE id_="+rec; // consulta para eliminar recordatorio
		SQLiteDatabase db = getWritableDatabase(); // abre la base en modo escritura
		db.execSQL(deleteQuery); // ejecuta la consulta
		
		return;
	}
	
	public void eliminarMascota(int rec){
		// no posee bloque try-catch, se debe hacer al momento de invocar el metodo
		String deleteQuery = "DELETE FROM mascotas WHERE idMascota="+rec; // consulta para eliminar recordatorio
		SQLiteDatabase db = getWritableDatabase(); // abre la base en modo escritura
		db.execSQL(deleteQuery); // ejecuta la consulta
		
		deleteQuery = "DELETE FROM recordatorio WHERE mascota="+rec; // consulta para eliminar recordatorio
		db.execSQL(deleteQuery); // ejecuta la consulta
		
		return;
	}
	
	/**
	 * se ejecuta al crear la instancia OpenHelper
	 * */
	@Override public void onCreate(SQLiteDatabase db){
		
	}
	
	/**
	 * actualizacion de versiones de la base de datos
	 * */
	@Override public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
		//onCreate(db);
	}
	
	/**
	 * verifica si existe un recordatorio que necesite ser notificado
	 */
	public int isEvent(){
		try{
			
			final Calendar c = Calendar.getInstance(); // otiene una instancia de calendario
			
			int hour   = c.get(Calendar.HOUR_OF_DAY); // hora actual, formato 24 horas interno
			int minute = c.get(Calendar.MINUTE);	  // minutos actuales	
			int anio   = c.get(Calendar.YEAR);		  // anio actual 
			int mes    = c.get(Calendar.MONTH) + 1;   // mes actual
			int dia    = c.get(Calendar.DAY_OF_MONTH);//dia actual
			
			/* correccion de decenas para minutos */
			String minutestr ="";
			
			if(minute < 10) {
				minutestr = "0"+String.valueOf(minute);
			}else{
				minutestr = String.valueOf(minute);
			}
			
			/* correccion de decenas para horas */
			String hourstr ="";
			
			if(hour < 10) {
				hourstr = "0"+String.valueOf(hour);
			}else{
				hourstr = String.valueOf(hour);
			}
			
			/* correccion de decenas para dias */
			String diastr ="";
			
			if(dia < 10) {
				diastr = "0"+String.valueOf(dia);
			}else{
				diastr = String.valueOf(dia);
			}
			
			/* correccion de decenas para meses */
			String messtr ="";
			
			if(mes < 10) {
				messtr = "0"+String.valueOf(mes);
			}else{
				messtr = String.valueOf(mes);
			}
			
			
			String hora = hourstr + ":" + minutestr; // cadena para hora  
			
			String fecha = diastr + "/" + messtr + "/" + String.valueOf(anio); // cadena para fecha 
			
			SQLiteDatabase db = getWritableDatabase(); // obtiene acceso de escritura a la base de datos
			
			// encontramos un recordatorio que cumpla que: el recordatorio debe ser lanzado la fecha de hoy, y se ha llegado la hora del dia para lanzarlo
			// este metodo se ejecuta cada segundo, por lo que limitamos al tratamiento de un recordatorio a la vez
			Cursor cursor = db.rawQuery("SELECT * FROM recordatorio WHERE hora='"+hora+"' AND fecha_i='"+fecha+"' LIMIT 1", null);
			
			int id = 0;       // id del recordatorio
			int ct = 0;       // contador de verificacion de recordatorio
			int interval = 0; // intervalo de dias para la siguiente notificacion
			
			Date d = null;	   // fecha actual
			Date n = null;     // nueva fecha
			
			Calendar cal = GregorianCalendar.getInstance();
			
			cal.set(anio, mes - 1,dia);
			
			d = cal.getTime();
			
			while(cursor.moveToNext()){
				id       = cursor.getInt(0); // obtiene el id
				interval = cursor.getInt(6); // obtiene el intervalo
				ct++; // aumenta el contador
			}
			
			// si existe un recordatorio
			if(ct > 0){
			
				n = sumarFechasDias(d, interval); // suma los dias a la fecha anterior para obtener la nueva fecha del recordatorio
				
				// formatea la nueva fecha
				DateFormat fechaHora = new SimpleDateFormat("dd/MM/yyyy", new Locale("es")); 
				String nfi = fechaHora.format(n);
				
				// actualiza con la nueva fecha
				db.execSQL("UPDATE recordatorio SET fecha_i='"+nfi+"' WHERE id_='"+id+"'");
			  
				return id;
				
			}else{ return -1; }
			
		}catch(Exception e){
			
		}
		
		return -1; // si se ejecuta esta setencia, todo fallo, no se notifica nada
	}
	
	public String getReminder(int id){
		try{
			SQLiteDatabase db = getWritableDatabase();  // acceso de escritura a la base de datos
			// obtiene un recordatorio basado en su id
			Cursor cursor = db.rawQuery("SELECT descripcion FROM recordatorio WHERE id_="+id, null);
			
			String nombre = "";
			
			while(cursor.moveToNext()){
				
				nombre = cursor.getString(0); // obtiene el nombre
			}
			
        	return nombre;
		
		}catch(Exception e){
			
			return "No se pudo obtener el recordatorio :("; // en caso de que algo en la bd llegue a fallar
		}
	}
	
	/**
	 * Funcion para almacenar un nuevo recordatorio
	 * */
	public void nuevoRecordatorio(int mascota, String desc, String fi, String ff,String h, int n){
		// no posee bloque try-catch, se debe hacer al momento de invocar el metodo
		int id = 0;
		id = ultimoRecordatorio() + 1; // simula auto-incremento
		String insertQuery = "INSERT INTO recordatorio VALUES("+id+","+mascota+",'"+desc+"','"+fi+"','"+ff+"','"+h+"',"+n+")";
		SQLiteDatabase db = getWritableDatabase(); // ontiene acceso de escritura
		db.execSQL(insertQuery); // inserta
		
		return;
	}
	
	/**
	 * Funcion para sumar n dias a una fecha determinada
	 * */
	private static java.util.Date sumarFechasDias(java.util.Date fch, int dias) {
        Calendar cal = new GregorianCalendar();
        cal.setTimeInMillis(fch.getTime());
        cal.add(Calendar.DATE, dias);
        return new java.util.Date(cal.getTimeInMillis());
    }
	
	/**
	 * Obtiene el id del ultimo recordatorio ingresado
	 * */
	public int ultimoRecordatorio(){
		// no posee bloque try-catch, se debe hacer al momento de invocar el metodo
		SQLiteDatabase db = getWritableDatabase(); // acceso de escritura
		Cursor cursor = db.rawQuery("SELECT id_ FROM recordatorio ORDER BY id_ ASC", null);
		int id = 0;
		while(cursor.moveToNext()){
			id = cursor.getInt(0); // obtiene el ultimo id ingresado
		}
		
		// retorna ultimo id de recordatorio
		return id;
	}
	
	
	/**
	 * Obtiene el nombre de una mascota determinada
	 * */
	public String obtenerNombre(int id_mascota){
		// no posee bloque try-catch, se debe hacer al momento de invocar el metodo
		try{
			SQLiteDatabase db = getWritableDatabase(); // acceso de escritura
			Cursor cursor = db.rawQuery("SELECT nombre FROM mascotas WHERE idMascota="+id_mascota, null);
			String nombre = "";
			
			while(cursor.moveToNext()){
				nombre = cursor.getString(0); // obtiene el nombre
			}
		
			return nombre;
			
		}catch(Exception e){
			
			return "";
		}
	}
	
	/**
	 * Obtiene todos los recordatorios relacionados con una mascota determinada
	 * */
	public ArrayList<Integer> obtenerRecordatorios(int id_mascota){
		SQLiteDatabase db = getWritableDatabase();
		Cursor cursor = db.rawQuery("SELECT id_ FROM recordatorio WHERE mascota="+id_mascota, null);
		ArrayList<Integer> recs = new ArrayList<Integer>();
		
		while(cursor.moveToNext()){
			recs.add(cursor.getInt(0)); // agrega los id de recordatorio a un ArratList
		}
        
        return recs;
	}
	
	/**
	 * Actualiza informacion sobre un recordatorio determinado
	 * */
	public void getData(int id_recordatorio){
		SQLiteDatabase db = getWritableDatabase(); // acceso de escritura
		Cursor cursor = db.rawQuery("SELECT * FROM recordatorio WHERE id_="+id_recordatorio, null);
		
		// actualiza datos
		while(cursor.moveToNext()){
			descripcion = cursor.getString(2);
			f_final     = cursor.getString(4);
			hora        = cursor.getString(5);
			intervalo   = cursor.getInt(6);
		}
        
		return;
	}
}
