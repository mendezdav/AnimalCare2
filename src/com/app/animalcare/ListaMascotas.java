package com.app.animalcare;

import android.graphics.Bitmap;

public class ListaMascotas {
	private int _idMascota;
	private Bitmap pic;
	private String _nombre;
	private String _apodo;
	private String _genero;
	public ListaMascotas(Bitmap pic, int _idMascota, String _nombre, String _apodo,
			String _genero) {
		super();
		this.pic = pic;
		this._idMascota = _idMascota;
		this._nombre = _nombre;
		this._apodo = _apodo;
		this._genero = _genero;
	}
	public int get_idMascota() {
		return _idMascota;
	}
	public void set_idMascota(int _idMascota) {
		this._idMascota = _idMascota;
	}
	public String get_nombre() {
		return _nombre;
	}
	public void set_nombre(String _nombre) {
		this._nombre = _nombre;
	}
	public String get_apodo() {
		return _apodo;
	}
	public void set_apodo(String apodo) {
		this._apodo = _apodo;
	}
	public String get_genero() {
		return _genero;
	}
	public void set_genero(String _genero) {
		this._genero = _genero;
	}
	public Bitmap getPic() {
		return pic;
	}
	public void setPic(Bitmap pic) {
		this.pic = pic;
	}
	
}
