package com.app.animalcare;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("ShowToast")
public class NuevaMascota extends ActionBarActivity {

	private TextView Output;
	private int estadoFoto=0; // foto por defecto
	private int year;
	private int month;
	private int day;
	static final int DATE_PICKER_ID = 1111; 
	private final String ruta_fotos = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/animalCare/";
    private File file = new File(ruta_fotos);
    private ImageView image;
    private String rutaIMG;
    
    // por defecto sera la ruta de la iamgen por defecto
    private String fileRuta;
  	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nueva_mascota);
		/**
		 * Estableciendo objeto para preferencia
		 */
		final SharedPreferences pref = getPreferences(MODE_PRIVATE);
		final TextView propietario = (TextView) findViewById(R.id.txtPropietario);
		
		propietario.setText(pref.getString("PROPIETARIO", ""));
		
		 /**
		  * Llenando Data Spinner genero
		  * 
		  */
		 Spinner sp = (Spinner)findViewById(R.id.comboGenero);
		 ArrayAdapter adapter = ArrayAdapter.createFromResource(this, R.array.genero, android.R.layout.simple_spinner_item);
		 adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 sp.setAdapter(adapter);
		 
		 /**
		  * Obtener dia actual
		  */
		 TextView txtNacimiento = (TextView) findViewById(R.id.txtNacimiento);
		 Calendar c = Calendar.getInstance();
		 SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
		 txtNacimiento.setText( formato.format(c.getTime()) );
		 
		 /**
		  * Llenando Data Spinner especie
		  * 
		  */
		 Spinner spEspecie = (Spinner)findViewById(R.id.comboEspecie);
		 ArrayAdapter adapterEspecie = ArrayAdapter.createFromResource(this, R.array.especie, android.R.layout.simple_spinner_item);
		 adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		 spEspecie.setAdapter(adapterEspecie);
		 // ACCION SPINNER ESPECIE
		 spEspecie.setOnItemSelectedListener(new OnItemSelectedListener() {
	            @Override
	            public void onItemSelected(AdapterView<?> parent,
	    	            android.view.View v, int position, long id) {
	            	String[] items; 
	                // TODO Auto-generated method stub
	                Object item = parent.getItemAtPosition(position);
	                int asignado = 0;
	                if (item!=null) {
	                	//Toast.makeText(NuevaMascota.this, item.toString(), Toast.LENGTH_SHORT).show();
                		// crear adaptador para cargar razas
                		Spinner spRaza = (Spinner)findViewById(R.id.comboRaza);
                		 ArrayAdapter<String> adapterRaza = null;
		                if (item.equals("Perro"))
		                {
		                	items = getResources().getStringArray(R.array.perros);
		                	adapterRaza =new ArrayAdapter<String>(NuevaMascota.this, android.R.layout.simple_spinner_item, items);
		                	asignado = 1;
		                }
		                if (item.equals("Gato"))
		                {
		                	items = getResources().getStringArray(R.array.gatos);
		                	adapterRaza =new ArrayAdapter<String>(NuevaMascota.this, android.R.layout.simple_spinner_item, items);
		                	asignado=1;
		                }
		                if (item.equals("Ave"))
		                {
		                	items = getResources().getStringArray(R.array.Ave);
		                	adapterRaza =new ArrayAdapter<String>(NuevaMascota.this, android.R.layout.simple_spinner_item, items);
		                	asignado=1;
		                }
		                if (item.equals("Reptil"))
		                {
		                	items = getResources().getStringArray(R.array.Reptil);
		                	adapterRaza =new ArrayAdapter<String>(NuevaMascota.this, android.R.layout.simple_spinner_item, items);
		                	asignado=1;
		                }
		                if (item.equals("Roedor"))
		                {
		                	items = getResources().getStringArray(R.array.Roedor);
		                	adapterRaza =new ArrayAdapter<String>(NuevaMascota.this, android.R.layout.simple_spinner_item, items);
		                	asignado=1;
		                }
		                   
	                	if (asignado == 1){
		                adapterRaza.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	               		spRaza.setAdapter(adapterRaza);}
	                }
	            }
	            @Override
	            public void onNothingSelected(AdapterView<?> arg0) {
	                // TODO Auto-generated method stub
	            }
	        });
		 
		 /// prueba guardar en BD
		 Button guardar = (Button) findViewById(R.id.guardar);
		 guardar.setOnClickListener(new OnClickListener (){
		 public void onClick( View v){
			// llamar funcion para validar si todos los datos fueron ingresados
			 String estadoVerificacion = verificarDatos();
			 if (estadoVerificacion.equals("")){
				 
			  DatabaseHandler db = new DatabaseHandler(NuevaMascota.this);
			  // referenciando
			  Spinner genero = (Spinner) findViewById(R.id.comboGenero);
			  Spinner especies = (Spinner) findViewById(R.id.comboEspecie);
			  Spinner raza = (Spinner) findViewById(R.id.comboRaza);
			  
			  TextView nombre = (TextView) findViewById(R.id.txtNombre);
			  TextView apodo = (TextView) findViewById(R.id.txtApodo);
			  TextView nac = (TextView) findViewById(R.id.txtNacimiento);
			  
			  /**Estableciendo la preferencia**/
			  SharedPreferences.Editor edit = pref.edit();
			  edit.putString("PROPIETARIO", propietario.getText().toString());
			  edit.commit();
			// copiando imagen selecciona desde galeria a la carpeta adecuada
			  if (estadoFoto == 3 ){
			  Bitmap bm = BitmapFactory.decodeFile(rutaIMG);
			  String extStorageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/animalCare/";
			  File file = new File(extStorageDirectory, getCode() + ".jpg");
			  FileOutputStream outStream;
			  try {
			  outStream = new FileOutputStream(file);
			  bm.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
			  try {
			  outStream.flush();
			  outStream.close();
			  } catch (IOException e) {
			  // TODO Auto-generated catch block
			  e.printStackTrace();
			  }

			  } catch (FileNotFoundException e) {
			  // TODO Auto-generated catch block
			  e.printStackTrace();
			  }
			  }
			  // fin
			  if (estadoFoto != 3)
			  rutaIMG = fileRuta;
			  
			  if (estadoFoto==0)
			  {
			  rutaIMG = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +
			  "/animalCare/foto_default.jpg";
			  }
			  
			  //String rutaImg = fileRuta;
			  if (estadoFoto==0)
			  {
				  rutaIMG = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + 
						  "/animalCare/foto_default.jpg";
			  }
			 
			  
			  if(!(propietario.getText().toString().trim()=="" ||  nombre.getText().toString().trim()=="" || apodo.getText().toString().trim()=="") ){
			  
				  db.addContact(new Mascotas(propietario.getText().toString(),
					  		nombre.getText().toString(), apodo.getText().toString(),
					  		genero.getSelectedItem().toString(), nac.getText().toString(),
					  		especies.getSelectedItem().toString(),
					  		raza.getSelectedItem().toString(),rutaIMG));
				  
				 Context context   = getApplicationContext();
				 CharSequence text = "Perfil creado!"; 
				 int duration      = Toast.LENGTH_SHORT;
				 Toast success = Toast.makeText(context, text, duration);
				 success.show(); 
				 
				  Intent i = new Intent(getApplicationContext(), AnimalCare.class);
				  startActivity(i);
			  }else{
				  Context context   = getApplicationContext();
			      CharSequence text = "Por favor, complete todos los datos"; 
				  int duration      = Toast.LENGTH_SHORT;
			      Toast success = Toast.makeText(context, text, duration);
				  success.show();
			  }
		  }
		 // fin if
		 else{
			  
			 Toast.makeText(getApplicationContext(), estadoVerificacion, Toast.LENGTH_LONG).show();
		 }
			  
		 }});
		 
		Output = (TextView) findViewById(R.id.txtNacimiento);
		final Calendar c2 = Calendar.getInstance();
		year  = c2.get(Calendar.YEAR);
		month = c2.get(Calendar.MONTH);
		day   = c2.get(Calendar.DAY_OF_MONTH);
		// Mostrar fecha actual
		Output.setText(new StringBuilder()
				// Month is 0 based, just add 1
				.append(month + 1).append("-").append(day).append("-")
				.append(year).append(" "));
		// Boton para escuchar date picker dialog
		Output.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
                // On button click show datepicker dialog 
				showDialog(DATE_PICKER_ID);
			}
		});
		
		/**
		 * Programando boton de toma foto
		 */
		 // creando directorio sino existe
		 file.mkdirs();
		 //copiando img default
		 Bitmap bm = BitmapFactory.decodeResource( getResources(), R.drawable.foto_default2);
		 String extStorageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/animalCare/";
		 File file = new File(extStorageDirectory, "foto_default.jpg");
		 FileOutputStream outStream;
		try {
			outStream = new FileOutputStream(file);
			bm.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
			 try {
				outStream.flush();
				outStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 image= (ImageView) findViewById(R.id.imageViewFoto);
		 image.setOnClickListener(new OnClickListener (){
				@Override
				public void onClick(View v) {
					/*fileRuta = ruta_fotos + getCode() + ".jpg";
					File mi_foto = new File( fileRuta );
					try {
		                mi_foto.createNewFile();
		            } catch (IOException ex) {	            	
		            	Log.e("ERROR ", "Error:" + ex);
		            }       
		            //
		            Uri uri = Uri.fromFile( mi_foto );
		            //Abre la camara para tomar la foto
		            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
  		            //Guarda imagen
		            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
		            //Retorna a la actividad
		            startActivityForResult(cameraIntent, 0);*/
					selectImage();
				}
			});
	
	} // fin on create
	
	
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_PICKER_ID:
			return new DatePickerDialog(this, pickerListener, year, month,day);
		}
		return null;
	}

	private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			
			year  = selectedYear;
			month = selectedMonth;
			day   = selectedDay;

			// Show selected date 
			Output.setText(new StringBuilder().append(month + 1)
					.append("-").append(day).append("-").append(year)
					.append(" "));
	
		   }
	    };
	    
		@SuppressLint("SimpleDateFormat")
		private String getCode()
		{
			DatabaseHandler db = new DatabaseHandler(this);
			int id = db.getUltimoID();
			id++;
			return "perfil_" + String.valueOf(id);		
			 
		}
		
		/*
		 * MODIFICACION SELECCIONAR FOTO
		 */
	      private void selectImage() {
	        final CharSequence[] options = {"Tomar Foto","Seleccionar Galeria","Cancelar" };
	        AlertDialog.Builder builder = new AlertDialog.Builder(NuevaMascota.this);
	        builder.setTitle("Agregar Foto");
	        builder.setItems(options, new DialogInterface.OnClickListener() {
	            @Override
	            public void onClick(DialogInterface dialog, int item) {
	                if (options[item].equals("Tomar Foto"))
	                {
	                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	                    File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/animalCare/", 
	                    		getCode() + ".jpg");
	                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
	                    startActivityForResult(intent, 1);

	                }
	                else if (options[item].equals("Seleccionar Galeria"))
	                {
	                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
	                    startActivityForResult(intent, 2);
	                }
	                else if (options[item].equals("Cancelar")) {
	                    dialog.dismiss();
	                }
	            }
	        });
	        builder.show();
	    }
	      
	    public boolean onCreateOptionsMenu(Menu menu) {
	          // Inflate the menu; this adds options to the action bar if it is present.
	          getMenuInflater().inflate(R.menu.main, menu);
	          return true;
	    }
		 /*	@Override
	    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    	super.onActivityResult(requestCode, resultCode, data);
	    	File imgFile = new  File(fileRuta);
            if(imgFile.exists()){

                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                ImageView myImage = (ImageView) findViewById(R.id.imageViewFoto);
                myImage.setImageBitmap(myBitmap);
                estadoFoto = 1; // se cambio foto por defecto
            }
	 	}*/
	    
	    public String verificarDatos()
	    {
	    boolean estadoVerificacion=true;
	    int datoFaltante=0;

	    TextView propietario = (TextView) findViewById(R.id.txtPropietario);
	    TextView nombre = (TextView) findViewById(R.id.txtNombre);
	    TextView apodo = (TextView) findViewById(R.id.txtApodo);

	    // comenzando verificacion

	    if (TextUtils.isEmpty(propietario.getText().toString()))
	    {
	    datoFaltante = 1;
	    estadoVerificacion=false;
	    }
	    if (TextUtils.isEmpty(nombre.getText().toString()) && estadoVerificacion==true)
	    {
	    datoFaltante = 2;
	    estadoVerificacion=false;
	    }
	    if (TextUtils.isEmpty(apodo.getText().toString()) && estadoVerificacion==true)
	    {
	    datoFaltante = 3;
	    estadoVerificacion=false;
	    }



	    Log.d("estado", " sd: " + estadoVerificacion);

	    if (!estadoVerificacion){
	    Log.d("entra", "entra if flase");
	    String mensaje="Debe ingresar ";
	    switch (datoFaltante){
	    case 1:	
	    mensaje+=" propietario";
	    break;
	    case 2:	
	    mensaje+=" nombre";
	    break;
	    case 3:	
	    mensaje+=" apodo";
	    break;
	    }
	    return mensaje;
	    }else
	    {
	    Log.d("entra", "entra if else");
	    return "";
	    }

	    }
	    
	    @Override
	    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);

	            
	    if (resultCode == RESULT_OK) {
	    	 if (requestCode == 1) {
	    		 fileRuta = ruta_fotos + getCode() + ".jpg";
	    		 File f = new File(fileRuta);
	    		 if(f.exists()){
	    		 Bitmap myBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());
	    		 ImageView myImage = (ImageView) findViewById(R.id.imageViewFoto);
	    		 myImage.setImageBitmap(myBitmap);
	    		 estadoFoto = 1; // se cambio foto por defecto
	    		 }
	    	 }
	    else if (requestCode == 2) {
		    Uri selectedImage = data.getData();
		    String[] filePath = { MediaStore.Images.Media.DATA };
		    Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);
		    c.moveToFirst();
		    int columnIndex = c.getColumnIndex(filePath[0]);
		    String picturePath = c.getString(columnIndex);
		    c.close();
		    Bitmap thumbnail = (BitmapFactory.decodeFile(picturePath));
		    rutaIMG = picturePath;
		    image.setImageBitmap(thumbnail);
		    estadoFoto=3;
	    }
	  }

	}  
}
