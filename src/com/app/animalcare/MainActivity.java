package com.app.animalcare;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class MainActivity extends ActionBarActivity {

	private int mascota_activa; // mascota
	private String nombre_mascota;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		
		mascota_activa = getIntent().getExtras().getInt("id_mascota"); // pasado por activity padre
		
		Log.d("Id mascotas recibido ", getIntent().getExtras().getInt("id_mascota") + "");
		
		MascotaDB helperDB = MascotaDB.getInstance(this); // manejador de base de datos
		
		setTitle("Nuevo recordatorio"); // cambio titulo de activity
		
		nombre_mascota = helperDB.obtenerNombre(mascota_activa); // obtener nombre de mascota activa
		
		/* referencia a views de la activity */
		EditText fecha_inicio = (EditText)findViewById(R.id.fecha_inicio);
		EditText fecha_fin    = (EditText)findViewById(R.id.fecha_fin);
		EditText mascota      = (EditText)findViewById(R.id.mascota);
		EditText hora         = (EditText)findViewById(R.id.txtHora);
		EditText descripcion  = (EditText)findViewById(R.id.txtDesc);
		
		mascota.setText("Para: "+nombre_mascota); // mostramos al usuario el nombre de la mascota como referencia
		
		String personalizado = getIntent().getExtras().getString("recordatorio");
		descripcion.setText(personalizado);     //Estableciendo la descripcion
		// evento para campo descripcion
		descripcion.setOnKeyListener(new OnKeyListener(){
			@Override public boolean onKey(View v, int keyCode, KeyEvent event){
				
				// cada vez que se suelte una tacla
				if(event.getAction() == KeyEvent.ACTION_UP){
					
					TextView actual = (TextView) findViewById(R.id.lblcactuales); // contador de caracteres
					EditText desc   = (EditText) v;     // descripcion
					int len = desc.getText().length();  // longitud de la descripcion
					actual.setText(String.valueOf(len));//muestra la cantidad de caracteres digitados
				}
				
				/* retornar falso deja habilitada la escritura */
				return false;
			}
		});
		
		
		/* evitar que se pueda modificar el campo */
		fecha_inicio.setOnKeyListener(new OnKeyListener(){
			@Override public boolean onKey(View v, int keyCode, KeyEvent event){
				
				return true;
			}
		});
		
		/* evitar que se pueda modificar el campo */
		fecha_fin.setOnKeyListener(new OnKeyListener(){
			@Override public boolean onKey(View v, int keyCode, KeyEvent event){
				
				return true;
			}
		});
		
		/* evitar que se pueda modificar el campo */
		mascota.setOnKeyListener(new OnKeyListener(){
			@Override public boolean onKey(View v, int keyCode, KeyEvent event){
				
				return true;
			}
		});
		
		/* evitar que se pueda modificar el campo */
		hora.setOnKeyListener(new OnKeyListener(){
			@Override public boolean onKey(View v, int keyCode, KeyEvent event){
				
				return true;
			}
		});
	}

	/** almacenar un nuevo recordatorio en la base de datos */
	public void guardarRecordatorio(View v){
		
		/* datos para toasMessage */
		Context context   = getApplicationContext();
		CharSequence text = "Guardado con exito"; 
		int duration      = Toast.LENGTH_SHORT;
		
		/* intento de almacenado de datos */
		try{
			
			/* obtiene los datos de las views */
			String descripcion  = nombre_mascota + ": " + ((EditText) findViewById(R.id.txtDesc)).getText().toString();
			String fecha_inicio = ((EditText) findViewById(R.id.fecha_inicio)).getText().toString();
			String fecha_fin    = ((EditText) findViewById(R.id.fecha_fin)).getText().toString();
			String hora         = ((EditText) findViewById(R.id.txtHora)).getText().toString();
			int n_dias          = Integer.parseInt(((EditText) findViewById(R.id.n_dias)).getText().toString());
			
			if(descripcion.trim()=="" || fecha_inicio.trim()=="" || fecha_fin.trim()=="" || hora.trim()=="" || n_dias <=0){
				
				text = "Por favor, rellene todos los campos"; // verifica que no existan campos vacios
			
			}else{
			
				MascotaDB helperDB    = MascotaDB.getInstance(this); // obtiene el manejador de la base de datos
				
				/* inserta el nuevo recordatorio con ayuda del manejador */
				helperDB.nuevoRecordatorio(mascota_activa, descripcion, fecha_inicio, fecha_fin,hora, n_dias);
				
				// abre una activity con la lista de recordatorios
				Intent recAct = new Intent(this, ListaRecordatorio.class);
				recAct.putExtra("mascota", mascota_activa); // pasa como parametro la mascota activa
				startActivity(recAct); // inicia activity
				
			}
			
		}catch(Exception e){
			
			text = "Por favor, rellene todos los campos"; // si sucede algun error, generalmente cuando algun "parse" falla
		}
		
		/* Muestra mensaje toast con el estado de la operacion */
		Toast success = Toast.makeText(context, text, duration);
		success.show();
	}
	
	/** evento lanzado con un click, muestra la activity con la lista de recordatorios */
	public void verRecordatorios(View v){
		Intent recAct = new Intent(this, ListaRecordatorio.class);
		recAct.putExtra("mascota", mascota_activa);
		startActivity(recAct);
	}
	
	/** evento que se puede lanzar manualmente o desde actionBar, muestra la activity con la lista de recordatorios */
	public void verRecordatorios(){
		Intent recAct = new Intent(this, ListaRecordatorio.class);
		recAct.putExtra("mascota", mascota_activa);
		startActivity(recAct);
	}
	
	/** crea el menu en el actionBar */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu); // referencia a main.xml
		return true;
	}
	
	
	/** Muestra un dialogo para seleccion de fecha */
	public void showDatePickerDialog(View v) {
	    DialogFragment newFragment = new DatePickerFragment(); // crea el fragment
	    Bundle args = new Bundle(1); // crea un Bundle para paso de argumentos
	    args.putInt("sView", v.getId()); // se envia el id de la view que llamo al fragment
	    newFragment.setArguments(args); // se establecen los argumentos
	    newFragment.show(getSupportFragmentManager(), "datePicker"); // se muestra el dialogo, ojo, version de compatibilidad
	}
	
	/** Muestra un dialogo para seleccion de hora */
	public void showTimePickerDialog(View v) {
	    DialogFragment newFragment = new TimePickerFragment(); // crea el fragment
	    Bundle args = new Bundle(1); // crea un Bundle para paso de argumentos
	    args.putInt("sView", v.getId()); // se envia el id de la view que llamo al fragment
	    newFragment.setArguments(args);// se establecen los argumentos
	    newFragment.show(getSupportFragmentManager(), "timePicker"); // se muestra el dialogo, ojo, version de compatibilidad
	}
	
	/** implementacion del selector de fecha */
	public static class DatePickerFragment extends DialogFragment
	implements DatePickerDialog.OnDateSetListener {
	
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Usa la fecha actual como fecha inicial
			final Calendar c = Calendar.getInstance();
			int year         = c.get(Calendar.YEAR);
			int month        = c.get(Calendar.MONTH);
			int day          = c.get(Calendar.DAY_OF_MONTH);
			
			// crea una nueva instancia del selector de fecha y la retorna
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}
		
		/* evento de seleccion de fecha */
		public void onDateSet(DatePicker view, int year, int month, int day) {
			/* Escribiendo la fecha en wl view que invoca al fragment */
			
			month++; // debe sumarse uno al mes, mes inicia en 0
			
			String diastr =""; // cadena para correccion de dias < 10
			
			if(day < 10) {
				diastr = "0"+String.valueOf(day); // se agrega un cero para completar las dos cifras
			}else{
				diastr = String.valueOf(day);
			}
			
			String messtr =""; // cadena para correccion de mese < 10
			
			if(month < 10) {
				messtr = "0"+String.valueOf(month); // se agrega un creo para completar las dos cifras
			}else{
				messtr = String.valueOf(month);
			}
			
			// se crea la cadena para la fecha
			String fecha = diastr + "/" + messtr + "/" + String.valueOf(year); 
			
			int field = getArguments().getInt("sView"); // se obtiene el id de la view de los argumentos pasados en el bundle
			EditText f = (EditText) getActivity().findViewById(field); // se obtiene la view por medio de su id
			f.setText(fecha); // se pone la fecha en la view correspondiente
		}
		
	}
	
	/** implementacion del selector de hora */
	public static class TimePickerFragment extends DialogFragment
	implements TimePickerDialog.OnTimeSetListener {
		
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			
			Context context = getActivity(); // obtener contexto donde se ejecuta el fragment
			
			// usar hora actual como hora corregida
			final Calendar c = Calendar.getInstance();
			int hora         = c.get(Calendar.HOUR_OF_DAY);
			int minutos      = c.get(Calendar.MINUTE);
			
			// crea una nueva instancia de selector de hora y retorna
			// true indica que trabajamos en formato 24 horas internamente para evitar lidiar con AM y PM
			return new TimePickerDialog(context, this,hora,minutos,true);
		}
		
		/** evento de seleccion de hora */
		public void onTimeSet(TimePicker view, int hour, int minute) {
			/* escribimos la hora en la view que creo el fragment */
			
			String minutestr =""; // cadena de correccion para minutos < 10
				
			if(minute < 10) {
				minutestr = "0"+String.valueOf(minute); // agregamos un creo para completar las dos cifras
			}else{
				minutestr = String.valueOf(minute);
			}
			
			String hourstr =""; // cadena de correccion para horas < 10
			
			if(hour < 10) {
				hourstr = "0"+String.valueOf(hour); // agregamos un creo para completar las dos cifras
			}else{
				hourstr = String.valueOf(hour);
			}
			
			String hora = hourstr + ":" + minutestr; 
			
			// se crea la cadena para la hora
			int field   = getArguments().getInt("sView"); // se obtiene el id de la view de los argumentos pasados en el bundle
			EditText f  = (EditText) getActivity().findViewById(field); // se obtiene la view por medio de su id
			f.setText(hora);  // se pone la fecha en la view correspondiente
		}
	}
	
	/** evento que se ejecuta cuando se selecciona un item del actionBar (menu) */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		
		int id = item.getItemId(); // id de item seleccionado 
		
		// se selecciona settins
		if (id == R.id.action_settings) {
			return true;
		}
		
		// muestra listado de mascotas
		if(id == R.id.action_verlistado){
			verRecordatorios();
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

}
