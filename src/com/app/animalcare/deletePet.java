package com.app.animalcare;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class deletePet extends DialogFragment{
	 @Override public Dialog onCreateDialog(Bundle savedInstance){
		 
		 AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		 
		 builder.setMessage(R.string.del_str)
		 		.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
						((AnimalCare)getActivity()).eliminarMascota();
					}
		 		})
		 		.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						
					}
		 		});
		 
		 return builder.create();
	 }
}
