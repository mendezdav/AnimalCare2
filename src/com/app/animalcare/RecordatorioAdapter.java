package com.app.animalcare;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class RecordatorioAdapter extends ArrayAdapter<Integer>{
	
	private final Context context;
	private Integer[] values;
	
	public RecordatorioAdapter(Context context, Integer[] values){
		super(context, R.layout.lista_rec, values);
		this.context = context;
		this.values = values;
	}
	
	@Override public View getView(int position, View convertView, ViewGroup parent){
		LayoutInflater inflater  = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.lista_rec, parent, false);
		int id_recordatorio = values[position];
		MascotaDB helperDB    = MascotaDB.getInstance(getContext());
		try{
			helperDB.getData(id_recordatorio);
		}catch(Exception e){
			// NADA
		}
		String metadatastr = "cada "+helperDB.obtenerIntervalo()+" dias - hasta: "+helperDB.obtenerFechaFinal();
		TextView desc = (TextView) rowView.findViewById(R.id.descripcion);
		TextView meta = (TextView) rowView.findViewById(R.id.metadata_lisrec);
		TextView reg  = (TextView) rowView.findViewById(R.id.id_reg);
		desc.setText(helperDB.obtenerDescripcion());
		meta.setText(metadatastr);
		reg.setText(String.valueOf(id_recordatorio));
		return rowView;
	}

}
