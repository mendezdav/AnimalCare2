package com.app.animalcare;

import java.util.ArrayList;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.os.Build;

public class MenuRecordatorio extends ActionBarActivity {
	private int id_mascota;
	private String recordatorio;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("Recordatorios");
		setContentView(R.layout.activity_menu_recordatorio);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		/**
		 * Definiendo propiedades
		 */
		final String[] opc_predefinidas = {"Consulta al veterinario", "Medicinas", "Vacunas", "Baño", "Personalizado"};
		ListView lvRecordatorios = (ListView) findViewById(R.id.lvMenuRecordatorios);
		
		ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, R.layout.options_list, R.id.optionItem,opc_predefinidas);
		/**Obteniendo Intent**/
		id_mascota = getIntent().getExtras().getInt("id_mascota");
		lvRecordatorios.setAdapter(adaptador);
		
		lvRecordatorios.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int posicion,
					long arg3) {
				Intent new_recordatorio = new Intent(getApplicationContext(), MainActivity.class);
				Log.e("Posicion: ",posicion + "");
				Log.e("Item:", opc_predefinidas[posicion]);
				
				new_recordatorio.putExtra("id_mascota", id_mascota);
				if(posicion == 4)									/**Enviando el elemento de la lista**/
					new_recordatorio.putExtra("recordatorio", "");
				else
					new_recordatorio.putExtra("recordatorio", opc_predefinidas[posicion]);
				startActivity(new_recordatorio);
			}	
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_recordatorio, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(
					R.layout.fragment_menu_recordatorio, container, false);
			return rootView;
		}
	}

}
